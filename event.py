from selenium import webdriver
import re
from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask_cors import CORS


def fb_crawler(driver, url):
    driver.get(url)  # Get the Event page and execute JS --> get Ajax data

    # Title is always there and have an id --> simple
    try:
        titre = driver.find_element_by_xpath("//h1[@id='seo_h1_tag']").text
    except:
        titre = ''

    # Event dates are located in ISO 8601 format in "content" attribute
    try:
        dates = driver.find_element_by_class_name('_5xhk').get_attribute("content").split(' to ')
        start_date = dates[0]
        end_date = dates[1]
    except:
        start_date = ''
        end_date = ''

    # Crawling description is much more creapy as xPath change randomly at each load
    # The best way to find description is to count <span> in the Reaction_units : description is the third one
    # First, we try to deploy description by clicking "See more..."
    # Facebook has two different "See more...", chosen randomly. Only inline "See more..." is creapy, as we don't want
    # to get the "See more..." text. That's why we click on it. The other one is outside our span element.
    try:
        see_more = driver.find_elements_by_xpath('//*[@id="reaction_units"]//span')[2].find_element_by_class_name('see_more_link')
        see_more.click()
    except:
        pass
    # Then we turn <br> in \n and remove html tags
    description = driver.find_elements_by_xpath('//*[@id="reaction_units"]//span')[2].text
    #description = re.sub('<br>', '\n', description)
    #description = re.sub('<[^>]*>', '', description)

    # Address is simple
    try:
        address = driver.find_element_by_css_selector('li._3xd0').find_element_by_css_selector('div._5xhp').text
    except:
        address = ''

    # Image is simple
    try:
        image = driver.find_element_by_css_selector('div#event_header_primary').find_element_by_css_selector('img').get_attribute("src")
    except:
        image = ''

    # To get latitude and longitude, we use the small map. Coordinates are hidden in the image URL.
    # We search for it by a simple REGEX
    try:
        map = driver.find_element_by_css_selector('div.fbPlaceFlyoutWrap').find_element_by_css_selector('img._a3f').get_attribute("src")
        coord = re.search('markers=(.+?)&', map).group(1).split('%2C')
        lat = coord[0]
        long = coord[1]
    except:
        lat = ''
        long = ''

    # Venue is simple
    try:
        venue = driver.find_element_by_css_selector('li._3xd0').find_element_by_css_selector('a._5xhk').text
    except:
        venue = ''

    result = {
        'titre': titre,
        'venue': venue,
        'address': address,
        'latitude': lat,
        'longitude': long,
        'start_date': start_date,
        'end_date': end_date,
        'image': image,
        'description': description
    }
    '''
    for key in result:
        print(key + ' : ')
        print(result[key])
        print(' ')
    '''
    return result


# This function is used to launch an headless chromium browser to crawl event data
def get_event(url):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('headless')  # Used to run script without launching Chrome UI
    chrome_options.add_argument('window-size=1200x600')  # Needed to be able to click on page
    driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)  # Don't forget to get Chromedriver

    try:
        response = fb_crawler(driver, url)
    except:
        response = {'error': 'impossible to get event data'}
    finally:
        driver.quit()
    return response


# This class describes the get-fb-event endpoint
class FbEvent(Resource):
    def post(self):
        url = request.json['url']
        return jsonify(get_event(url))


if __name__ == '__main__':
    app = Flask(__name__)
    CORS(app)
    api = Api(app)
    api.add_resource(FbEvent, '/get-fb-event')
    app.run(port='5002')



